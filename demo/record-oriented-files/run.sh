#!/usr/bin/env bash
set -e
set -x

mkdir -p build 
cd build
cmake -G 'Unix Makefiles' ..
make
./record_oriented_files 10
