#include <stdio.h>
#include <malloc.h>

int main() {
    int n = 1000, s = 1000;

    printf("**** Allocating %d small blocks\n", n);
    int k; for (k=0; k<n; ++k) {void* small = calloc(1, s);}

    printf("**** Allocating 1 large block\n");
    void* large = calloc(n, s);
    return 0;
}

