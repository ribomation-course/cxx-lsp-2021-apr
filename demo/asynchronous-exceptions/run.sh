#!/usr/bin/env bash
set -e
set -x

mkdir -p build
cd build
cmake -G 'Unix Makefiles' ..
make
./catch-nullptr 4
