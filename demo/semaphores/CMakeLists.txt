cmake_minimum_required(VERSION 3.5)
project(semaphores)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fmax-errors=1 -Wall -Wextra ")

add_executable(ping-pong
  semaphore.hxx
  ping-pong.cxx
)
target_link_libraries(ping-pong pthread)
