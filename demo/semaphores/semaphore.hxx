#pragma once

#include <string>
#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>

namespace ribomation {
    namespace concurrent {
        using namespace std;

        class Semaphore {
            const string name;
            sem_t* sema;
            bool owner;

        public:
            Semaphore(const string& name, int count)
                    : name{name[0] == '/' ? name : '/' + name} {
                sema = sem_open(name.c_str(), O_CREAT | O_EXCL, S_IRUSR | S_IWUSR, count);
                if (sema == SEM_FAILED)
                    throw string("failed to create FS semapahore: ") + name;
                owner = true;
            }

            Semaphore(const char* name) {
                sema = sem_open(name, 0);
                if (sema == SEM_FAILED)
                    throw string("failed to open FS semapahore: ") + name;
                owner = false;
            }

            ~Semaphore() {
                sem_close(sema);
                if (owner)
                    sem_unlink(name.c_str());
            }

            Semaphore& notOwner() {
                owner = false;
                return *this;
            }

            void wait() { sem_wait(sema); }

            void signal() { sem_post(sema); }
        };
    }
}
