
#include <iostream>
#include <iomanip>
#include <string>
#include <ctime>
#include "tcp-socket.hxx"

using namespace std;
using namespace ribomation::tcp;

int main(int numArgs, char* args[]) {
    int port = 10000;

    for (auto k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-p") {
            port = static_cast<unsigned short>(stoi(args[++k]));
        }
    }

    ServerSocket srv{port};
    cout << "server waiting: port=" << port << endl;
    SocketStream client;
    srv.wait(client);
    time_t t  = time(nullptr);
    tm     tm = *localtime(&t);
    client << "time: " << put_time(&tm, "%F %T") << endl;

    return 0;
}
