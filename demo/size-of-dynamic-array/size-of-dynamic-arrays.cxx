#include <iostream>
#include <iomanip>
#include <string>
#include <bitset>
#include <new>
using namespace std;


struct Account {
    static int  cnt;
    long double balance = 0.0;

    Account() { ++cnt; }
    ~Account() { --cnt; }
    void operator =(int n) { balance = n; }
};

int Account::cnt = 0;

int main(int argc, char** argv) {
    cout << "Account::cnt = " << Account::cnt << endl;
    auto N = argc > 1 ? stoi(argv[1]) : 42U;

    auto arr = new Account[N];
    cout << "Account::cnt = " << Account::cnt << endl;

    for (auto k = 0U; k < N; ++k) arr[k] = k + 1;
    for (auto k = 0U; k < N; ++k) cout << arr[k].balance << " ";
    cout << endl;

    auto ArraySize = ( reinterpret_cast<unsigned long*>(arr) )[-1];
    cout << "Array Size: " << dec << ArraySize<< endl;

    delete[] arr;
    cout << "Account::cnt = " << Account::cnt << endl;

    return 0;
}