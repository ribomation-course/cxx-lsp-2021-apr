# Installation Instructions

In order to participate and perform the programming exercises of the course, 
you need to have the following installed.

* Zoom Client (*not applicable for classroom course*)
  - https://us02web.zoom.us/download
  - [Video/Audio Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)
* GIT Client
  - https://git-scm.com/downloads
* A (Ubuntu) Linux system
* C/C++ compiler
* CMake, Make


Installation of Ubuntu Linux
----
We will do the exercises on Linux. Therefore you need access to a Linux or Unix system, preferably Ubuntu.
Read our common guide of how to install Linux on WSL and a C++ compiler.

* [Linux and C++ Installation](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/linux-and-cxx.md)


Installation within *NIX
----
Within Linux; you need to install (_the commands below are for Ubuntu_)
* The Make / CMake builder tools; `sudo apt install make cmake`
* The GNU C/C++ compiler, version 9 or later; `sudo apt install gcc-9 g++-9` or `sudo apt install gcc-10 g++-10`
* Git client; `sudo apt install git`
* Any text editor you like, such as `nano`, `emacs`, `vim`


Suggestions for editor/IDE
====

You also need to use a text editor or IDE to write your programs. If you are already familiar with tools like Emacs or Eclipse/C++, please go ahead and install them too. On the other hand, if you would like some advise I do recommend to choose one of these two suggestions:

* If you just want a decent text editor, then go for Text Editor (gedit). It's already installed on Ubuntu and you can launch it from the start menu in the upper left corner or from a terminal window with the command: `gedit my-file.cpp &`

* If you want to run a good IDE instead, then download and install the trial version of JetBrains CLion from <https://www.jetbrains.com/clion/>
This is my choice of C++ IDE and I will be using it during the course.
Within Ubuntu Desktop; open the Ubuntu Software app, search for `clion` and install it.

* Another, interesting IDE/smart-editor is MS Visual Code, which exists for Linux as well. <https://code.visualstudio.com/>


Running CLion on Windows 10 and use WSL for compilation and execution
----
My favorite C++ IDE is CLion and it works very nicely with WSL; i.e. you install
CLion on Windows but use the compiler resources within WSL. Just follow the 
instructions below. In short, you will run a BASH script that installs the
SSH daemon, then you configure CLion to connect via SSH to WSL.
* [How to Use WSL Development Environment in CLion](https://www.jetbrains.com/help/clion/how-to-use-wsl-development-environment-in-clion.html)
* [Using WSL toolchains in CLion on Windows (YouTube)](https://youtu.be/xnwoCuHeHuY)

