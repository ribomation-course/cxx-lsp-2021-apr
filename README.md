# Linux Systems Programming using C++
### 26 - 28 April 2021
### 10 - 12 May 2021

# Links
* [Installation instructions](./installation-instructions.md)
* [Linux and C++ Installation](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/linux-and-cxx.md)
* [Zoom Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)  (*not applicable for classroom course*)
* [Course Details](https://www.ribomation.se/courses/cxx/cxx-systems-programming)


# Course GIT Repo
It's recommended that you keep the git repo and your solutions separated. 
Create a dedicated directory for this course and a sub-directory for 
each chapter. Get the course repo initially by a `git clone` operation

    mkdir -p ~/lsp-course/my-solutions
    cd ~/lsp-course
    git clone <git HTTPS clone link> gitlab

During the course, solutions will be push:ed to this repo and you can get these by
a `git pull` operation

    cd ~/lsp-course/gitlab
    git pull


Build Demo and Solution Programs
====

The solutions and demo programs are all using CMake as the build tool. CMake is a cross
plattform generator tool that can generate makefiles and other build tool files. It is also
the project descriptor for JetBrains CLion, which is my IDE of choice for C/C++ development.

However, You don't need to use CLion in order to compile and run the sources. 
What you do need to have, are `cmake`, `make` and `gcc/g++` installed. 

Inside a directory with a solution or demo, run the following commands to build the program.
This will create the executable in the build directory.

    mkdir build
    cd build
    cmake -G 'Unix Makefiles' ..
    cmake --build .


Interesting Links
====
Here are some links of interesting stuff.

* [Hello World from Scratch (ACCU 2019)](https://youtu.be/MZo7k_IOCe8)
* [Rich Code for Tiny Computers: A Simple Commodore 64 Game in C++17 (CppCon 2016)](https://youtu.be/zBkNBP00wJE)
* [An Introduction to Custom Allocators (ACCU 2019)](https://youtu.be/IGtKstxNe14)
* [Program Library HOWTO](http://tldp.org/HOWTO/Program-Library-HOWTO/index.html)
* [Filesystem Hierarchy Standard](http://www.pathname.com/fhs/pub/fhs-2.3.html)
* [Compiler Explorer](https://godbolt.org/)
* [C++ Insights](https://cppinsights.io/)
* [C++ Quick Benchmarks](http://quick-bench.com/)
* [Coliru - Online Compiler](https://coliru.stacked-crooked.com/)
* [WandBox - Online Compiler](https://wandbox.org/)
* [Building GCC 9](https://solarianprogrammer.com/2016/10/07/building-gcc-ubuntu-linux/)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

