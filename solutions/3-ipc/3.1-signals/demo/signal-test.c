//COMPILE: c99 -g -Wall signal_test.c -o signal-test
//RUN    : ./signal-test &
//INTERACT: kill -<signal> <pid>
//    e.g.: kill -SEGV 1234

#include <stdio.h>
#include <unistd.h>

int main() {
	printf("Waiting for something to happen...\n");
	pause();
	printf("GOT a signal\n");
	return 0;
}
