#pragma once

constexpr auto MAX_PAYLOAD = 128U;
struct Text {
    unsigned size;
    char     payload[MAX_PAYLOAD];
};
