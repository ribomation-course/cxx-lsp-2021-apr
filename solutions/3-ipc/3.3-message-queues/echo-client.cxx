#include <iostream>
#include <algorithm>
#include <cstring>
#include "message-queue.hxx"
#include "echo.hxx"

using namespace std;
using namespace std::string_literals;
using namespace ribomation::mq;


int main(int argc, char** argv) {
    MessageQueue<Text> req{"echo-req", false};
    MessageQueue<Text> res{"echo-res", false};

    for_each(argv + 1, argv + argc, [&](string msg) {
        Text txt{};
        txt.size = static_cast<unsigned>(msg.size());
        msg.copy(txt.payload, msg.size(), 0);
        req << txt;

        res >> txt;
        string reply{txt.payload, txt.size};
        cout << "[client] recv: '" << reply << "'" << endl;
    });

    Text quit{};
    "QUIT"s.copy(quit.payload, 4, 0);
    quit.size = 4;
    req << quit;

    return 0;
}
