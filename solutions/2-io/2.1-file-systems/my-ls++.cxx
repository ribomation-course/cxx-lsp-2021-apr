#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>
#include <stdexcept>
#include <cstring>
#include <cerrno>
#include <time.h>
#include <sys/stat.h>
#include <dirent.h>
using namespace std;
using namespace std::literals;

class Entry {
    dirent* payload = nullptr;
    struct stat info = {};

    string toDateTime(const timespec* ts) const {
        tm   data;
        char buf[64];
        if (localtime_r(&ts->tv_sec, &data) == nullptr) {
            throw invalid_argument{"failed to invoke localtime_r(3): "s + strerror(errno)};
        }
        strftime(buf, sizeof(buf), "%F %T", &data);
        return {buf, strlen(buf)};
    }

public:
    Entry(dirent* payload) : payload{payload} {
        if (lstat(payload->d_name, &info) == -1) {
            throw invalid_argument{"failed to invoke stat(2) on file "s
                                + payload->d_name + ": "s + strerror(errno)};
        }
    }

    Entry(const Entry& that) : payload{that.payload} {
        if (lstat(payload->d_name, &info) == -1) {
            throw invalid_argument{"failed to invoke stat(2) on file "s
                                + payload->d_name + ": "s + strerror(errno)};
        }
    }

    ~Entry() = default;
    auto operator=(const Entry&) -> Entry& = default;

    string  name()          const { return payload->d_name; }
    unsigned size()         const { return info.st_size; }
    string  modifiedAt()    const { return toDateTime(&info.st_mtim); }
    string  createdAt()     const { return toDateTime(&info.st_ctim); }
    bool    isFile()        const { return payload->d_type == DT_REG; }
    bool    isDirectory()   const { return payload->d_type == DT_DIR; }
    bool    isSoftLink()    const { return payload->d_type == DT_LNK; }

    string type() const {
#ifdef _DIRENT_HAVE_D_TYPE
        switch (payload->d_type) {
            case DT_BLK: return "Block device";
            case DT_CHR: return "Char device";
            case DT_DIR: return "Directory";
            case DT_FIFO: return "Fifo";
            case DT_LNK: return "Sym. link";
            case DT_REG: return "Normal file";
            case DT_SOCK: return "Socket";
            case DT_UNKNOWN: return "Unknown";
        }
#endif
        return "DT_* not supported";
    }
};

class Directory {
    DIR* dir;

public:
    explicit Directory(const string& path) : dir{opendir(path.c_str())} {
        if (dir == nullptr) {
            throw invalid_argument("failed to open dir '" + path + "': " + strerror(errno));
        }
    }
    ~Directory() { closedir(dir); }

    struct iterator {
        Directory& directory;
        dirent last = {};
        bool   done = false;

        void next() {
            if (done) return;
            dirent* status;
            auto rc = readdir_r(directory.dir, &last, &status);
            if (rc == 0) {
                done = (status == nullptr);
            } else {
                throw invalid_argument{"failed to invoke readdir(3): "s + strerror(rc)};
            }
        }

        iterator(Directory& dir) : directory{dir} { next(); }
        iterator(Directory& dir, bool done) : directory{dir}, done{done} {}

        bool  operator !=(const iterator& ) { return !done; }
        void  operator ++() { next(); }
        Entry operator *() { return {&last}; }
    };

    iterator begin() { return {*this}; }
    iterator end()   { return {*this, true}; }
};

void listDir(const string& path) {
    auto      dir = Directory{path};
    for (auto entry : dir) {
        cout << setw(20) << left << entry.name() + (entry.isDirectory() ? "/"s : ""s) << " "
             << setw(12) << left << entry.type() << " "
             << setw(8) << right << entry.size() << " bytes "
             << "[" << entry.modifiedAt() << "]"
             << endl;
    }
}

int main(int argc, char** argv) {
    cout.imbue(locale{"en_US.UTF8"});
    if (argc == 1) {
        listDir("."s);
    } else {
        for_each(argv + 1, argv + argc, [](string path) { listDir(path); });
    }
    return 0;
}
