#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace std::literals;

int main(int argc, char** argv) {
    auto vec = vector<string>{"anna"s, "berit"s, "carin"s};
    for (const auto& name : vec) {
        cout << name << endl;
    }
    cout << "vec[2]: " << vec[2] << endl;
    cout << "vec.at(2): " << vec.at(2) << endl;
   // cout << "vec[3]: " << vec[3] << endl;
    cout << "vec.at(3): " << vec.at(3) << endl;

    return 0;
}
