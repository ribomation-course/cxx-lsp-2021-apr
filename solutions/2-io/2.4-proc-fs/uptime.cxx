#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <stdexcept>
#include <chrono>
#include <sys/sysinfo.h>

using namespace std;
using namespace std::literals;
using namespace std::chrono;

int numCPUs() {
    auto cpuInfo = ifstream{"/proc/cpuinfo"};
    if (!cpuInfo) throw runtime_error("Cannot open /proc/cpuinfo");

    int    cpuCount = 0;
    for (string line; getline(cpuInfo, line);) {
        if (line.find("processor") != string::npos) cpuCount++;
    }

    return cpuCount;
}

string toTimeString(long seconds) {
    const long minute = 60;
    const long hour   = 60 * minute;
    const long day    = 24 * hour;

    ostringstream buf;
    buf << setw(2) << setfill('0') << (seconds % day) / hour << ':';
    buf << setw(2) << setfill('0') << (seconds % hour) / minute << ':';
    buf << setw(2) << setfill('0') << seconds % minute;

    return buf.str();
}

int numCPUsAlt() { return get_nprocs(); }

int main() {
    auto uptime = ifstream{"/proc/uptime"};
    if (!uptime) throw runtime_error("Cannot open /proc/uptime");

    double boot, idle;
    uptime >> boot >> idle;
    idle /= numCPUsAlt();

    cout << "Time since boot: " << toTimeString(static_cast<long>(boot)) << endl;
    cout << "Idle time: " << toTimeString(static_cast<long>(idle)) << endl;

    return 0;
}
