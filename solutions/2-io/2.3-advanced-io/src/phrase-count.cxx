#include <iostream>
#include <iomanip>
#include <algorithm>
#include <chrono>
#include "MemoryMappedFile.hxx"

using namespace std;
using namespace std::literals;
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using stopWatch = std::chrono::high_resolution_clock;

auto count(string_view haystack, string phrase) {
    auto count = 0UL;

    for (auto pos = 0UL; (pos = haystack.find(phrase.data(), pos)) != string_view::npos; pos += phrase.size())
        ++count;

    return count;
}

int main(int numArgs, char* args[]) {
    auto filename = "./data/shakespeare.txt"s;
    auto phrase   = "Hamlet"s;

    for (auto k = 1; k < numArgs; ++k) {
        auto arg = string{args[k]};
        if (arg == "-p") phrase = args[++k];
        else if (arg == "-f") filename = args[++k];
    }

    auto start   = stopWatch::now();
    {
        auto f = MemoryMappedFile{filename};
        cout << "Loaded " << setprecision(3) << f.bytes() / (1024.0 * 1024) << " MB from " << filename << endl;
        cout << "Phrase '" << phrase << "' occurs " << count(f.data(), phrase) << " times" << endl;
    }
    auto elapsed = stopWatch::now() - start;
    cout << "Elapsed " << duration_cast<milliseconds>(elapsed).count() * 1E-3 << " seconds\n";

    return 0;
}
