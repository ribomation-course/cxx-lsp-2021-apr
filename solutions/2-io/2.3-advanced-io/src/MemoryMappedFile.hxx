#include <string>
#include <string_view>
#include <stdexcept>
#include <cstring>

#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

using namespace std;

class MemoryMappedFile {
    const string filename;
    char* payload;
    size_t payloadSize;

    static bool exists(const string& file) {
        struct stat fileInfo{};
        if (stat(file.c_str(), &fileInfo) == 0)
            return true;
        if (errno == ENOENT) return false;

        throw runtime_error("Cannot stat '" + file + "': " + strerror(errno));
    }

    static size_t size(const string& file) {
        struct stat fileInfo{};
        if (stat(file.c_str(), &fileInfo) == 0)
            return static_cast<size_t>(fileInfo.st_size);
        throw runtime_error("Cannot stat '" + file + "': " + strerror(errno));
    }

    void map(const string& file, size_t size, bool nullTerminate = false) {
        int fd = open(file.c_str(), O_RDWR);
        if (fd < 0) throw invalid_argument("cannot open '" + file + "': " + strerror(errno));

        payloadSize = size;
        payload     = (char*) mmap(0, size, PROT_READ | (nullTerminate ? PROT_WRITE : 0), MAP_PRIVATE, fd, 0);
        if (payload == MAP_FAILED) throw runtime_error(string("failed to mmap: ") + strerror(errno));

        if (nullTerminate) {
            (reinterpret_cast<char*>(payload))[payloadSize - 1] = '\0';
        }

        close(fd);
    }

public:

    /**
     * Opens an <em>existing</em> record-oriented file and maps it into a memory segment
     * @param filename
     * @throws invalid_argument     if file is not found
     */
    MemoryMappedFile(const string& filename, bool nullTerminate = false) : filename(filename) {
        if (!exists(filename)) throw invalid_argument(filename + " not found");

        map(filename, size(filename), nullTerminate);
    }

    /**
     * Disposes the memory segment
     */
    virtual ~MemoryMappedFile() {
        munmap(payload, payloadSize);
    }

    /**
     * Size of the segment in bytes
     * @return
     */
    size_t bytes() const {
        return payloadSize;
    }

    /**
     * Returns the segment as an array of bytes
     * @return
     */
    string_view data() const {
        return {payload, payloadSize};
    }

    MemoryMappedFile() = delete;
    MemoryMappedFile(const MemoryMappedFile&) = delete;
    MemoryMappedFile& operator =(const MemoryMappedFile&) = delete;
};

