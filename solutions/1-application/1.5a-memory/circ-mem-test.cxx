#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <random>
#include <type_traits>
#include "circular-memory.hxx"

using namespace std;
using namespace ribomation::memory;

template<typename T>
auto toString(T* ptr) -> string {
    auto buf = ostringstream{};
    if constexpr(is_integral_v<T>) {
        buf << dec;
    } else if constexpr(is_floating_point_v<T>) {
        buf << fixed << setprecision(3);
    } else {
        buf << "???";
    }
    buf << setw(12) << *ptr << " @ " << reinterpret_cast<unsigned long>(ptr);
    return buf.str();
}

int main(int argc, char** argv) {
    auto numTurns = (argc == 1) ? 100U : stoi(argv[1]);

    auto m          = CircularMemory<512>{};
    auto r          = random_device{};
    auto nextShort  = uniform_int_distribution<short>{10, 1'000};
    auto nextLong   = uniform_int_distribution<long>{10, 10'000};
    auto nextDouble = uniform_real_distribution<long double>{10, 100'000};

    for (auto k = 1U; k <= numTurns; ++k) {
        auto* ptr1 = new(m.alloc(sizeof(short))) short{nextShort(r)};
        auto* ptr2 = new(m.alloc(sizeof(long))) long{nextLong(r)};
        auto* ptr3 = new(m.alloc(sizeof(long double))) long double{nextDouble(r)};
        cout << setw(4) << k << ") " << toString(ptr1) << toString(ptr2) << toString(ptr3) << endl;
    }

    return 0;
}
