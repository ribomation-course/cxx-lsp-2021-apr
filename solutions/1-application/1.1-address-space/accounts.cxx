#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <unistd.h>

using namespace std;

// -------------------------
// --- Account
// -------------------------
class Account {
    int   balance = 42;
    float rate;

public:
    Account(int balance = 100, float rate = 0.025) : balance(balance), rate(rate) {
        cout << ">> Account @ " << address(this) << endl;
    }

    ~Account() {
        cout << "<< Account @ " << address(this) << endl;
    }

    string toString() const {
        ostringstream buf;
        buf << "Account{balance=" << balance << ", rate=" << rate << "} @ " << address((void*) this) << endl;
        return buf.str();
    }

private:
    string address(void* addr) const {
        auto buf = ostringstream{};
        buf << setw(16) << setfill('0') << dec << reinterpret_cast<unsigned long>(addr);
        return buf.str();
    }
};

ostream& operator <<(ostream& os, const Account& a) {
    return os << a.toString();
}


// -------------------------
// --- App
// -------------------------
void func(Account* heap) {
    cout << ">> func()" << endl;
    Account local{300, 0.02};

    cout << "STACK: " << local << endl;
    cout << "HEAP: " << *heap << endl;
    delete heap;

    cout << "<< func()" << endl;
}

Account global{200, 0.01};

int main() {
    cout << ">> main()" << endl;
    cout << "page.size = " << getpagesize() << " bytes" << endl;

    cout << "GLOBAL: " << global << endl;

    Account local{150, 0.1};
    cout << "STACK: " << local << endl;

    Account* heap = new Account{400, 0.03};
    func(heap);

    cout << "<< main()" << endl;
    return 0;
}
