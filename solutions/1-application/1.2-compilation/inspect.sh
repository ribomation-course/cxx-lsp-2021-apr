#!/bin/bash
set -eux

wc *.{ii,s}

nm --demangle --line-numbers --print-size --radix=d hello-dyn | egrep 'dataVar|bssVar'
