#!/bin/bash
set -eux

echo "-- hello-dyn --"
g++ -std=c++17 -Wall -save-temps -time greeting.cxx app.cxx -o hello-dyn

echo "-- hello-sta --"
g++ -std=c++17 -Wall --static -time greeting.cxx app.cxx -o hello-sta

size hello-{dyn,sta}

# much more timing stats with -ftime-report
# https://stackoverflow.com/questions/3025443/how-to-calculate-gcc-compilation-time
