#include <iostream>
#include <string>

extern std::string greeting();

long double dataVar = 3.141592654;
unsigned long long bssVar;

namespace {
  int multiple = 17;
}

int main() {
  using namespace std;

  cout << "Hello from a tiny C program. multiple=" << multiple << endl;
  cout << "func" << greeting() << endl;

  return 0;
}
