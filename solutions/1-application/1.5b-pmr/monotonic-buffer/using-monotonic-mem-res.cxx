#include <iostream>
#include <vector>
#include <algorithm>
#include <memory_resource>

using std::cout;
using std::endl;
using std::fill_n;
using std::begin;
using std::data;
using std::size;
using std::pmr::monotonic_buffer_resource;
using std::pmr::null_memory_resource;
using std::pmr::vector;

int main() {
    char storage[64] = {};
    fill_n(begin(storage), size(storage) - 1, '.');

    monotonic_buffer_resource    memory{data(storage), size(storage), null_memory_resource()};
    vector<char> v{&memory};
    //v.reserve(26);
    //v.push_back('#');
    for (auto ch = 'a'; ch <= 'z'; ++ch) v.push_back(ch);

    cout << "storage: \n[" << storage << "]\n";

    return 0;
}
