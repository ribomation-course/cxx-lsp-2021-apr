#include <string>
#include <vector>
#include <memory_resource>
#include <cstdlib>
#include <cstdio>
#include "allocation-tracker.hxx"

using std::byte;
using std::pmr::monotonic_buffer_resource;
using std::pmr::null_memory_resource;
using std::pmr::vector;
using std::pmr::string;


int main() {
    TrackNew::reset();
	TrackNew::trace(true);
    {
        byte       storage[112'888];
        monotonic_buffer_resource    pool{storage, sizeof(storage), null_memory_resource()};

        vector<string> sentences{&pool};
        const auto N     = 1000U;
        for (auto      k = 0U; k < N; ++k) {
            sentences.emplace_back("FooBar strikes back, and again");
        }
		for (auto& s : sentences) puts(s.data());
    }
    TrackNew::status();
    return 0;
}


