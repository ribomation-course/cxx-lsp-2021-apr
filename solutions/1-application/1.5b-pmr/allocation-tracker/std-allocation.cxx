#include <vector>
#include <string>
#include "allocation-tracker.hxx"

using std::vector;
using std::string;

int main() {
    TrackNew::reset();
    TrackNew::trace(true);
    {
        vector<string> sentences;
        for (auto k = 0U; k < 10U; ++k) {
            sentences.emplace_back("FooBar strikes back, and again");
        }
    }
    TrackNew::status();
    return 0;
}


