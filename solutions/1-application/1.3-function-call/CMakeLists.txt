cmake_minimum_required(VERSION 3.9)
project(function_call)

set(CMAKE_C_STANDARD 99)
add_compile_options(-Wall -Wextra -Wfatal-errors)

add_executable(varargs varargs.c)
add_executable(longjmp longjmp.c)
