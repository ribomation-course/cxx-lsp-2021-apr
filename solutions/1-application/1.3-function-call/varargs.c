//COMPILE: c99 -Wall -g varargs.c -o varargs
//RUN    : ./varargs

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

double average(int num, ...) {
    int sum = num;
    int cnt = 1;

    va_list args;
    va_start(args, num);  //void* args = &num
    int n;
    while ((n = va_arg(args, int)) != 0) { //n = (int)*args; args += sizeof(int)
        sum += n;
        ++cnt;
    }
    va_end(args);

    return (double) sum / cnt;
}

int main() {
    double m = average(2, 2, 4, 4, 6, 6, 8, 8, 0);
    printf("[main] avg=%g\n", m);

    m = average(1, -2, -3, 3, -2, 1, 0);
    printf("[main] avg=%g\n", m);

    m = average(5, 0);
    printf("[main] avg=%g\n", m);

    return 0;
}

